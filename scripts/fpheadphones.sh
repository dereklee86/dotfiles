#!/bin/sh


getcurrent="$(amixer -c 0 cget numid=14  | tail -1 )"

current="${getcurrent: -1}"

# switch to speakers
if [[ "$current" == "1" ]]
then
amixer -c 0 cset numid=14 2 -q
cp /home/derek/.pulse/equalizerrc.mb42  /home/derek/.pulse/equalizerrc
pulseaudio-equalizer disable >/dev/null
pulseaudio-equalizer enable >/dev/null

echo ""

# switch to fp headphones
elif [[ "$current" == "2" ]]
then
amixer -c 0 cset numid=14 1 -q
cp /home/derek/.pulse/equalizerrc.hd201 /home/derek/.pulse/equalizerrc
pulseaudio-equalizer disable >/dev/null
pulseaudio-equalizer enable >/dev/null

echo ""
fi
