#!/bin/sh

# default connection of qemu:///system has been set in local libvirt.conf:
# uri_default = "qemu:///system"

date
echo "starting VM.."

virsh start win7
ddccontrol dev:/dev/i2c-3 -r 0x60 -w 16 &>/dev/null
.screenlayout/single.sh

sleep 5

running=$(virsh list --state-running --name)

while [[ $running == *"win7"* ]] ; do
#echo -n "running: "
#date
running=$(virsh list --state-running --name)
sleep 15
done

date
echo "cleaning up.."

ddccontrol -f dev:/dev/i2c-3 -r 0x60 -w 17 &>/dev/null
.screenlayout/dual.sh

